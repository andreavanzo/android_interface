#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Andrea Vanzo'

#  Imports  #

# ROS imports
import rospy


# ROS msg and srv imports
from std_msgs.msg import String
from sensor_msgs.msg import Joy
from framenet_ros_msgs.msg import Motion
from framenet_ros_msgs.msg import ChangeDirection


# Python Libraries
import sys
import socket
import json


#  Variables  #
conn = None


#  Classes  #
class AndroidInterface(object):
    #  Interaction Variables  #
    reply = ''

    def __init__(self):
        global conn

        # Initialize node #
        rospy.init_node('android_interface_node', anonymous=True)
            
        # Reading params #
        self.port = int(rospy.get_param('~port', 9001))

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print 'Socket created'
            try:
                s.bind(('', self.port))
            except socket.error as msg:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
                sys.exit()
            print 'Socket bind complete'
            s.listen(10)

            print 'Socket now listening on port ' + str(self.port)
            
            # Initialize publishers #
            self.hypo_publisher = rospy.Publisher('/speech_hypotheses', String, queue_size=1)
            self.best_hypo_publisher = rospy.Publisher('/best_speech_hypothesis', String, queue_size=1)
            self.action_publishers = {}
            self.action_publishers[Motion.__name__] = rospy.Publisher('/motion', Motion, queue_size=10)
            self.action_publishers[ChangeDirection.__name__] = rospy.Publisher('/change_direction', ChangeDirection, queue_size=10)
            
            # Declare subscribers #
            rospy.Subscriber('/dialogue_manager_response', String, self.dm_response_callback, queue_size=1)
            rospy.Subscriber('/lu4r_response', String, self.lu4r_response_callback, queue_size=1)
            rospy.Subscriber('/joy', Joy, self.joy_callback, queue_size=10)
            rospy.Subscriber('/blind_controller_ack', String, self.blind_controller_callback, queue_size=1)

            current_fragment = ''
            while not rospy.is_shutdown():
                print 'Waiting for connection on port ' + str(self.port)
                conn, addr = s.accept()
                print 'Connected with ' + addr[0] + ':' + str(addr[1])
                while not rospy.is_shutdown():
                    data = conn.recv(2048)  # 512
                    print 'Received: ' + data
                    if data and not data.isspace():
                        if not data:
                            continue
                        if 'REQ' in data:
                            data = data.replace('REQ', '')
                            conn.send('ACK\n')
                            continue
                        if '$' in data:
                            current_fragment = data[1:-1]
                            print 'You selected the ' + current_fragment + ' fragment'
                            continue
                        if current_fragment == 'JOY':
                            print data
                        elif current_fragment == 'SLU':
                            transcriptions = json.loads(data)
                            self.best_hypo = transcriptions['hypotheses'][0]['transcription']
                            # TODO check whether LU4R is running, then
                            # TODO if lu4r is not running, publish in aiml topic with: self.best_hypo_publisher.publish(best_hypo)
                            self.hypo_publisher.publish(json.dumps(transcriptions))
                    else:
                        print 'Disconnected from ' + addr[0] + ':' + str(addr[1])
                        break
            s.close()
        except socket.error as socket_error:
            print('Error: ', socket_error)

    def lu4r_response_callback(self, lu4r_response):
        if lu4r_response.data.startswith('NO FRAME'):
            print 'NO FRAME(S) FOUND!'
            self.best_hypo_publisher.publish(self.best_hypo)
        else:
            actions = self.frame_to_aiml_format(lu4r_response.data)
            if len(actions) == 0:
                self.best_hypo_publisher.publish(self.best_hypo)
            for action in actions:
                try:
                    self.action_publishers[action.__class__.__name__].publish(action)
                except rospy.ROSSerializationException:
                    self.best_hypo_publisher.publish('BADMSG')

    def dm_response_callback(self, dm_response):
        if conn:
            conn.send(dm_response.data + '\n')
    
    def blind_controller_callback(self, bc_ack):
        self.best_hypo_publisher.publish(bc_ack.data)

    def frame_to_aiml_format(self, frame):
        output = []
        frames = frame.split('#')
        for frame in frames:
            frame_split = frame.split('(')
            if frame_split[0] == 'MOTION':
                frame_msg = Motion()
                fes_split = frame_split[1].split(',')
                frame_msg = self.fill_frame(frame_msg, fes_split)
                output.append(frame_msg)
            elif frame_split[0] == 'CHANGE_DIRECTION':
                frame_msg = ChangeDirection()
                fes_split = frame_split[1].split(',')
                frame_msg = self.fill_frame(frame_msg, fes_split)
                output.append(frame_msg)
        return output
    
    def fill_frame(self, frame_msg, fes_split):
        for fe in fes_split:
            fe_split = fe.split(':')
            role = fe_split[0]
            filler = fe_split[1]
            try:
                filler = float(filler.replace(',','.'))
            except ValueError:
                pass
            setattr(frame_msg, role, filler)
        return frame_msg

    def joy_callback(self, joy_msg):
        print joy_msg.buttons
        

#  If Main  #
if __name__ == '__main__':
    AndroidInterface()

