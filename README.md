# android_interface

The `android_interface` is the entry point of the *LU4R ROS interface*, acting as the actual orchestrator of the framework. It implements a TCP Server for the Android app and is coded as a Python node waiting for Client requests. Once a new request is received (i.e., a list of transcriptions for a given spoken sentence from the Android app), this module is in charge of extracting the perceived entities from a structured representation of the environment (i.e., the Semantic Map, a sub-component of the ). Then, whenever a running instance of is detected, the full list of hypotheses is published onto a dedicated topic (i.e., `/speech_hypotheses`). Otherwise, the best hypothesis is published onto the `/best_speech_hypothesis`.

This node is subscribed to two topics:

* `/lu4r_response`, containing the feedback provided by in terms of FrameNet frames, and
* `/dialogue_manager_response` that, instead, hosts the reply of a Dialogue Manager.

Depending on the response, the `android_interface` is then allowed to send a message back to the Android app.

The communication protocol requires the serialization of both speech hypotheses and entities of the Semantic Map into two different JSON objects (see [LU4R website](http://sag.art.uniroma2.it/lu4r.html) for more details). However, in order to obtain the desired interpretation, only the list of transcription is mandatory. In fact, even though environmental information is essential for the perception-driven interpretation, whenever it is not provided, the chain operates in a blind setting.

In addition, such node interfaces with a ROS-compliant navigation system; hence, through the virtual joypad coded into the Android App, it is possible to tele-operate the robot.

## Dependencies

The package depends on the following resources:

 * `rospy` [http://wiki.ros.org/rospy]
 
## Communication

The node communicates with ROS through publishers/subscribers over the following topics.

### Subscribers

 * `/dialogue_manager_response`: this topic contains the response from the dialogue manager.
 * `/lu4r_response`: this topic contains the response from LU4R.
 * `/joy`: this topic is used to trigger some event through the joypad.
 

### Publisher

 * `/speech_hypotheses`: all the speech hypotheses produced by the Android app ASR.
 * `/best_speech_hypothesis`: the best speech hypothesis produced by the Android app ASR.
 * `/motion`: contains the `Motion` ROS message corresponding to the *Motion* Frame, instantiated with all its frame elements, that needs to be executed by a robot controller.
 * `/change_direction`: contains the `ChangeDirection` ROS message corresponding to the *Change_direction* Frame, instantiated with all its frame elements, that needs to be executed by a robot controller.

## Running

### Params

 * `port`: the running port of the server socket.

### Startup

```
$ cd [YOUR_PATH]/android_interface
$ roscore
$ rosrun android_interface android_interface_node.py _port:=9001
```
